/obj/item/clothing/suit/antiquated_cape
	name = "antiquated cape"
	desc = "A some old and... familiar cape?"
	icon_state = "antiquated_cape"
	item_state = "ro_suit"
	item_color = "antiquated_cape"
	icon = 'icons/obj/clothing/infinity_work.dmi'
	worn_icon = 'icons/mob/infinity_work.dmi'

/obj/item/clothing/suit/hooded/black_hoody
	name = "black hoody"
	desc = "Warm black hoody."
	icon_state = "black_hoody"
	item_state = "bl_suit"
	item_color = "black_hoody"
	icon = 'icons/obj/clothing/infinity_work.dmi'
	worn_icon = 'icons/mob/infinity_work.dmi'
	hooded = 1
	togglename = "Toggle Hood"
	hoodtype = /obj/item/clothing/head/blackhoody

/obj/item/clothing/head/blackhoody
	name = "black hood"
	icon_state = "blackhoody"
	worn_icon = 'icons/mob/infinity_work.dmi'
	icon = 'icons/obj/clothing/infinity_work.dmi'
	body_parts_covered = HEAD
	flags = NODROP
	flags_inv = HIDEHAIR|HIDEEARS

/obj/item/clothing/suit/brand/blue_jacket
	name = "brand jacket"
	desc = "Brand jacket, maked by outerspace designer."
	icon_state = "brand_jacket"
	item_state = "b_suit"
	item_color = "brand_jacket"
	icon = 'icons/obj/clothing/infinity_work.dmi'
	worn_icon = 'icons/mob/infinity_work.dmi'

/obj/item/clothing/suit/brand/orange_jacket
	name = "brand orange jacket"
	desc = "Brand jacket, maked by outerspace designer."
	icon_state = "brand_orange_jacket"
	item_state = "o_suit"
	item_color = "brand_orange_jacket"
	icon = 'icons/obj/clothing/infinity_work.dmi'
	worn_icon = 'icons/mob/infinity_work.dmi'

/obj/item/clothing/suit/toggle/fiery_jacket
	name = "fiery jacket"
	desc = "A cool jacket, with fire logo on the back."
	icon_state = "fiery_jacket"
	item_state = "bl_suit"
	icon = 'icons/obj/clothing/infinity_work.dmi'
	worn_icon = 'icons/mob/infinity_work.dmi'
	togglename = "zipper"

/obj/item/clothing/suit/toggle/white_fiery_jacket
	name = "white fiery jacket"
	desc = "A cool white jacket, with blue fire logo on the back."
	icon_state = "white_fiery_jacket"
	item_state = "w_suit"
	icon = 'icons/obj/clothing/infinity_work.dmi'
	worn_icon = 'icons/mob/infinity_work.dmi'
	togglename = "zipper"

/obj/item/clothing/suit/leon_jacket
	name = "leon jacket"
	desc = "Light, leather jacket."
	icon_state = "leon_jacket"
	item_state = "bl_suit"
	icon = 'icons/obj/clothing/infinity_work.dmi'
	worn_icon = 'icons/mob/infinity_work.dmi'

/obj/item/clothing/suit/kimono
	name = "kimono"
	desc = "Some soft and comfortable kimono. Jack approved this!"
	icon_state = "kimono"
	item_state = "w_suit"
	icon = 'icons/obj/clothing/infinity_work.dmi'
	worn_icon = 'icons/mob/infinity_work.dmi'

/obj/item/clothing/suit/kimono/black
	name = "black kimono"
	desc = "Some soft and comfortable kimono."
	icon_state = "black_kimono"
	item_state = "bl_suit"

/obj/item/clothing/suit/longjacket
	name = "long jacket"
	desc = "Ordinary jacket. Just a little bit longer."
	icon_state = "longjacket"
	item_state = "bl_suit"
	icon = 'icons/obj/clothing/infinity_work.dmi'
	worn_icon = 'icons/mob/infinity_work.dmi'

/obj/item/clothing/suit/hooded/blue_fiery
	name = "blue jacket"
	desc = "A cool blue jacket."
	icon_state = "blue_fiery"
	item_state = "bl_suit"
	icon = 'icons/obj/clothing/infinity_work.dmi'
	worn_icon = 'icons/mob/infinity_work.dmi'
	hooded = 1
	togglename = "Toggle Hood"
	hoodtype = /obj/item/clothing/head/whitehoody

/obj/item/clothing/head/whitehoody
	name = "white hood"
	icon_state = "whitehoody"
	worn_icon = 'icons/mob/infinity_work.dmi'
	icon = 'icons/obj/clothing/infinity_work.dmi'
	flags = NODROP
	body_parts_covered = HEAD

/obj/item/clothing/suit/latex_top
	name = "latex top"
	desc = "Tight and skinny top."
	worn_icon = 'icons/mob/infinity_work.dmi'
	icon = 'icons/obj/clothing/infinity_work.dmi'
	icon_state = "latex_top"
	item_state = "bl_suit"

/obj/item/clothing/suit/handless_latex_top
	name = "handless latex top"
	desc = "Tight and skinny top."
	worn_icon = 'icons/mob/infinity_work.dmi'
	icon = 'icons/obj/clothing/infinity_work.dmi'
	icon_state = "handless_latex_top"
	item_state = "bl_suit"

/obj/item/clothing/suit/kimono/blue
	name = "blue kimono"
	desc = "Elegant blue kimono."
	icon_state = "blue_kimono"
	item_state = "b_suit"

/obj/item/clothing/suit/brand/brand_rjacket
	name = "brand red jacket"
	desc = "A some red, brand jacket from anyooh designer."
	icon_state = "brand_rjacket"
	item_state = "w_suit"
	item_color = "brand_rjacket"
	icon = 'icons/obj/clothing/infinity_work.dmi'
	worn_icon = 'icons/mob/infinity_work.dmi'

/obj/item/clothing/suit/brand/reg_jacket
	name = "white leather jacket"
	desc = "Light leather jacket, from anyooh designer."
	icon_state = "reg_jacket"
	item_state = "lb_suit"
	item_color = "reg_jacket"
	icon = 'icons/obj/clothing/infinity_work.dmi'
	worn_icon = 'icons/mob/infinity_work.dmi'

/obj/item/clothing/suit/wedding_dress
	name = "wedding dress"
	desc = "Oh holy! Such a nice white dress!"
	icon_state = "wedding_dress"
	item_color = "wedding_dress"
	item_state = "w_suit"
	worn_icon = 'icons/mob/infinity_work.dmi'
	icon = 'icons/obj/clothing/infinity_work.dmi'

/obj/item/clothing/suit/sc_labcoat
	name = "science labcoat"
	desc = "When you wear it, you feel like a real crazy scientist!"
	icon_state = "sc_labcoat"
	item_color = "sc_labcoat"
	item_state = "w_suit"
	worn_icon = 'icons/mob/infinity_work.dmi'
	icon = 'icons/obj/clothing/infinity_work.dmi'

/obj/item/clothing/suit/pullover
	name = "pullover"
	desc = "Soft and comfortable pullover. Just better than what your grandmother sewed."
	icon_state = "pullover"
	item_color = "pullover"
	item_state = "w_suit"
	worn_icon = 'icons/mob/infinity_work.dmi'
	icon = 'icons/obj/clothing/infinity_work.dmi'

/obj/item/clothing/suit/kimono/white
	name = "white kimono"
	desc = "Elegant white kimono."
	icon_state = "white_kimono"
	item_state = "w_suit"

/obj/item/clothing/suit/kimono/short_red
	name = "short kimono"
	desc = "Elegant short kimono."
	icon_state = "red_short_kimono"
	item_state = "r_suit"

/obj/item/clothing/suit/long_coat
	name = "long suit jacket"
	desc = "Just like the usual suit jacket, but longer."
	icon_state = "long_coat"
	item_state = "bl_suit"
	worn_icon = 'icons/mob/infinity_work.dmi'
	icon = 'icons/obj/clothing/infinity_work.dmi'